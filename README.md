Welcome to ClutchCoders!  
Every day each team member at least finish one problem.  
If anyone fails to submit one problem for two consective days, he will have to leave the group.  
Happy coding, everyone, fight for your dream job!  
Please fork and create a pull request to commit to the repo, thank you!  
Group GitBook: <https://qi7.gitbooks.io/clutchcoders/content/>  

10 Team Members: Accomplishments  
Daisy: 19  
EthanL: 16  
Qi Wang: 11  
Yu Zhang: 8  
Snow Sun: 7  
Liz Tan: 7  
Wei He: 7  
Xing: 6  
Jenny Zheng: 4  
Jessica: 4  
mrt: 1  

3.31 Qi 349 Daisy 349 Yu 349 Liz 349  
4.1 Qi 283 350 Yu 350 Snow 349 350 Liz 350 Daisy 94 144 145 173 350 Jenny 350 Xing 350 EthanL 349 350  
4.2 Qi 21 88 Snow 88 Yu 88 Liz 88 Jess 88 Wei 88 350  
4.3 Qi 92 206 Snow 206 Jenny 206 Liz 206 Yu 206 Xing 88 206 EthanL 88 206 150 Daisy 206  
4.4 Qi 141 Snow 141 Yu 141 EthanL 20 32 84 Jess 141 Daisy 141 Liz 141 Wei 141  
4.5 Qi 142 Snow 142 Daisy 142 173 349 435 Yu 142 Xing 141 142 Yu 142  
4.6 Qi 237 Jenny 142 237 Liz 142 237 Snow 237 Xing 237 mrt 237  
4.7 Qi 203 Jess 237 203  