// 92. Reverse Linked List II

// Reverse a linked list from position m to n. Do it in-place and in one-pass.

// For example:
// Given 1->2->3->4->5->NULL, m = 2 and n = 4,

// return 1->4->3->2->5->NULL.

// Note:
// Given m, n satisfy the following condition:
// 1 ≤ m ≤ n ≤ length of list.

import java.util.*;

class ListNode {
     int val;
     ListNode next;
     ListNode(int x) { val = x; }
 }

public class LC92{
	public ListNode reverseBetween(ListNode head, int m, int n) { // this is same thinking process as LC206, except in the end fix two more pointers, make the node at m-1 point to the node at n, make the node at point m+1 point to node n+1. 
        int i = 0;
        ListNode preHead = new ListNode(0);
        preHead.next = head;
        ListNode start = preHead;
        while(i < m - 1){ // go the node at m
            start = start.next;
            i++;
        }
        ListNode first = start.next;
        ListNode pre = first;
        ListNode curNode = pre.next;
        if(curNode == null){ // if the list contains only 1 node, return head
            return head;
        }
        ListNode tmp;
        
        int j = 0;
        while( j < n - m){ // reverse all the links between m and n
            tmp = curNode.next;
            curNode.next = pre;
            pre = curNode;
            curNode = tmp;
            j++;
        }
        
        start.next = pre; // make the node at m-1 point to n
        first.next = curNode; //make the node at m+1 point n+1
        
        return preHead.next;
    }

    public ListNode reverseBetween2(ListNode head, int m, int n){ //this is a totally different thinking process: every time place the node at m+1 between m-1 and m, do this n - m times, the order will be fixed.
    	ListNode preHead = new ListNode(0);
        preHead.next = head;
        ListNode start = preHead;
        int i = 0;
        while(i < m - 1){
            start = start.next; // start is the node at m-1
            i++;
        }
        ListNode first = start.next; // first is the node at m
        
        int j = 0;
        ListNode tmp;
        while(j < n - m){
            tmp = first.next; //remember the node after the first node
            first.next = first.next.next; //remove the node after the first node
            tmp.next = start.next; //insert the node right after the node m - 1: fixing the next pointer
            start.next = tmp; //insert the node right after the node m - 1: fixing the pre pointer
            j++;
        }
        return preHead.next;
    }
}
