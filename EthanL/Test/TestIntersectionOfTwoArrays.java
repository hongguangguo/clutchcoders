import org.junit.Assert;
import org.junit.Test;

/**
 * Created by demon on 2017/4/1.
 */
public class TestIntersectionOfTwoArrays {
    @Test
    public void testIntersection1() {
        IntersectionOfTwoArrays intersect = new IntersectionOfTwoArrays();
        int[] var1 = new int[]{1, 2, 2, 1};
        int[] var2 = new int[]{2, 2};
        int[] ans = new int[]{2};
        int[] result = intersect.intersection(var1,var2);
        Assert.assertEquals(ans, result);
        Assert.assertArrayEquals(ans, result);
    }
}
