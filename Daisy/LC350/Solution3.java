package LC350;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by lipingzhang on 4/1/17.
 */
public class Solution3 {
    public static int[] intersect(int[] nums1, int[] nums2) {
        // What if nums1's size is small compared to nums2's size? Which algorithm is better?
        if(nums1 == null || nums2 == null || nums1.length == 0 || nums2.length == 0){
            return new int[0];
        }

        Arrays.sort(nums1);
        Arrays.sort(nums2);
        List<Integer> list = new ArrayList<>();
        for (int i = 0, j = 0; i < nums1.length;) {
            int index = binarySearch(nums1[i], nums2);
            // count nums2
            int count2 = 0;
            while (index != -1 && index < nums2.length && nums2[index] == nums1[i]) {
                count2++;
                index++;
            }

            // count num1
            int count1 = 0;
            while (j < nums1.length && nums1[j] == nums1[i]) {
                count1++;
                j++;
            }

            for (int k = 0; k < Math.min(count1, count2); k++) {
                list.add(nums1[i]);
            }

            i = j;
        }


        int[] ans = new int[list.size()];
        int i = 0;
        for (int l : list) {
            ans[i++] = l;
        }

        return ans;
    }

    private static int binarySearch(int target, int[] nums){
        int l = 0;
        int r = nums.length - 1;
        while (l + 1 < r) {
            int m = l + (r - l) / 2;
            // from left to right to find first equal,  remove nums[m] == target branch for duplication cases
            if (nums[m] < target) {
                l = m;
            } else {
                r = m;
            }
        }

        if (nums[l] == target) {
            return l;
        } else if (nums[r] == target) {
            return r;
        } else {
            return -1;
        }
    }

    public static void main(String[] args){
        int[] n1 = {1, 2, 2, 2};
        int[] n2 = {2, 1, 2, 2, 6};

        int[] ans = intersect(n1, n2);

        for(int a : ans){
            System.out.println(a + "\n");
        }
    }

}


/*
What if elements of nums2 are stored on disk, and the memory is
limited such that you cannot load all elements into the memory at
once?
If only nums2 cannot fit in memory, put all elements of nums1 into a HashMap, read chunks of array that fit into the memory, and record the intersections.

If both nums1 and nums2 are so huge that neither fit into the memory, sort them individually (external sort), then read 2 elements from each array at a time in memory, record intersections.
 */