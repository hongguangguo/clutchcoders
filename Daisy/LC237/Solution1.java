package LC237;

/**
 * Created by lipingzhang on 4/6/17.
 */
class ListNode {
    int val;
    ListNode next;
    ListNode(int x) {
        val = x;
    }
}
public class Solution1 {
    public void deleteNode(ListNode node) {
        if (node == null) {
            return;
        }

        ListNode next1 = node.next;
        ListNode next2 = node.next.next;
        node.val = next1.val;
        node.next = next2;
    }
}
